import { showInputModal, showMessageModal } from './views/modal.mjs';
import { createElement, addClass, removeClass } from './helpers/domHelper.mjs';
import { appendRoomElement, updateNumberOfUsersInRoom, removeRoomElement } from './views/room.mjs';

const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });

const createRoom = () => {
	const name = window.prompt('Please enter the name of the room:');
	if (name) socket.emit('CREATE_ROOM', name);
}
const roomsPage = document.getElementById('rooms-page');
const roomsWrapper = document.getElementById('rooms-wrapper');
const gamePage = document.getElementById('game-page');
const createRoomBtn = document.getElementById('add-room-btn');

createRoomBtn.addEventListener('click', createRoom);

const addRoom = room => {
	return appendRoomElement({
		name: room.name,
		numberOfUsers: room.users.length,
		onJoin: () => {
			socket.emit('JOIN_ROOM', room.name);
		}
	});
};

const updateRooms = rooms => {
	roomsWrapper.innerHTML = '';
	rooms.map(addRoom);
};

const joinRoomDone = ({ room }) => {
	addClass(roomsPage, 'display-none');
	removeClass(gamePage, 'display-none');

	const userWrapper = document.getElementById('users-wrapper');
	const roomName = document.getElementById('room-name');
	const quitBtn = document.getElementById('quit-room-btn');
	const users = room.users.map(user => `
      <div class='user'>
        <div class='username'>
          <span class='ready-status-${user.ready ? 'green' : 'red'}'></span>
          ${user.username} ${user.username === username ? ' (you)' : ''}
        </div>
        <div class='user-progress-template ${user.username}'>
          <div class='user-progress' style='width:${user.progress}%'></div>
        </div>
      </div>`);

	const onLeaveRoom = () => {
		socket.emit("LEAVE_ROOM", room.name);
	};

	userWrapper.innerHTML = users;
	roomName.innerText = room.name;
	quitBtn.addEventListener('click', onLeaveRoom);
};

const leaveRoomDone = () => {
	addClass(gamePage, 'display-none');
	removeClass(roomsPage, 'display-none');
}

socket.on('UPDATE_ROOMS', updateRooms);
socket.on('JOIN_ROOM_DONE', joinRoomDone);
socket.on('LEAVE_ROOM_DONE', leaveRoomDone);
socket.on('ROOM_EXIST', () => {
	showMessageModal({
		message: 'Room with this name is alredy created',
	})
});
socket.on('connect_error', err => {
	showMessageModal({
		message: err.message,
		onClose: () => {
			sessionStorage.removeItem('username');
			window.location.replace('/login');
		}
	});
});