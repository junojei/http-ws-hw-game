import { Server, Socket } from 'socket.io';
import * as config from './config';

const rooms = new Map();
const maxUsers = config.MAXIMUM_USERS_FOR_ONE_ROOM;

export default (io: Server) => {
	io.use((socket: Socket, next) => {
		const username = socket.handshake.query.username;

		for (let [id, s] of io.of('/').sockets) {
			if (username === s.handshake.query.username
				&& socket.id !== id) {
				return next(new Error("User with this name is alredy connected"));
			};
		};
		next()

	})


	io.on('connection', (socket: Socket) => {
		const username = socket.handshake.query.username;

		const createRoom = name => {
			if (rooms.has(name)) {
				socket.emit("ROOM_EXIST");
			} else {
				rooms.set(name, { name, users: [] });
				joinRoom(name);
			}
		};

		const deleteRoom = name => {
			rooms.delete(name);
		}

		const joinRoom = roomId => {
			const room = rooms.get(roomId);
			if (room.users.length === maxUsers) {
				return;
			}
			const user = {
				username,
				progress: 0,
				ready: false
			}

			socket.join(roomId);

			rooms.set(roomId, { name: roomId, users: [...room.users, user] });

			io.to(socket.id).emit("JOIN_ROOM_DONE", { room: rooms.get(roomId) });
			io.to(roomId).emit("UPDATE_ROOM", { room: rooms.get(roomId) });
			socket.broadcast.emit("UPDATE_ROOMS", Array.from(rooms.values()));

		};

		const leaveRoom = roomId => {
			socket.leave(roomId);

			const room = rooms.get(roomId);
			const updateUsers = room.users.filter(user => user.username !== username);

			rooms.set(roomId, { name: roomId, users: updateUsers });

			io.to(socket.id).emit("LEAVE_ROOM_DONE");

			if (rooms.get(roomId)?.users?.length === 0) {
				deleteRoom(roomId);
			}

			io.to(socket.id).emit("UPDATE_ROOMS", Array.from(rooms.values()));
		}

		socket.on("CREATE_ROOM", createRoom);
		socket.on("JOIN_ROOM", joinRoom);
		socket.on("LEAVE_ROOM", leaveRoom);
		socket.emit("UPDATE_ROOMS", Array.from(rooms.values()));
	});
};
